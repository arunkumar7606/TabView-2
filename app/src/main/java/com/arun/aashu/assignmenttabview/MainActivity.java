package com.arun.aashu.assignmenttabview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TabHost tabHost;
    TabHost.TabSpec tabSpec;
    TextView tv1,tv2;
    EditText ed1,ed2;
    Button btn;
    RadioButton r1,r2;
    RadioGroup radioGroup;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tabHost=findViewById(R.id.tb);
        ed1=findViewById(R.id.edittext1);
        ed2=findViewById(R.id.edittext2);
        tv1=findViewById(R.id.textview1);
        tv2=findViewById(R.id.textview2);
        btn=findViewById(R.id.submitbutton);
        r1=findViewById(R.id.radioMale);
        r2=findViewById(R.id.radiofemale);
        radioGroup=findViewById(R.id.radiogroup);

        tabHost.setup();

        tabSpec=tabHost.newTabSpec("First Tab");
        tabSpec.setIndicator("Details");
        tabSpec.setContent(R.id.Details);
        tabHost.addTab(tabSpec);

        tabSpec=tabHost.newTabSpec("Second Tab");
        tabSpec.setIndicator("Fill the data");
        tabSpec.setContent(R.id.Result);
        tabHost.addTab(tabSpec);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String ab = ed1.getText().toString();
                String ac = ed2.getText().toString();



                if(ab.equals("") ||ac.equals("") ) {

                    ed1.setError("fill Name");
                    ed2.setError("fill phone number");

                }else{

                    tv2.setText("Name "+ab+ "\n" + "Phone no: "+ac);




                }

            }
        });








    }
}
